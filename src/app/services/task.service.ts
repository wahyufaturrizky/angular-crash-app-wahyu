import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from '../interface/Task';

const httpHeader = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  private apiUrl: string = 'http://localhost:5000';

  constructor(private http: HttpClient) {}

  getTask(): Observable<Task[]> {
    let res = this.http.get<Task[]>(this.apiUrl + '/data');
    return res;
  }

  deleteTask(task: Task): Observable<Task> {
    return this.http.delete<Task>(this.apiUrl + `/data/${task.id}`);
  }

  updateTask(task: Task): Observable<Task> {
    return this.http.put<Task>(
      this.apiUrl + `/data/${task.id}`,
      task,
      httpHeader
    );
  }

  postTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.apiUrl + '/data', task, httpHeader);
  }
}

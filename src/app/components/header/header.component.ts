import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  title = 'Task Tracker';
  showAddTask: boolean = false;

  constructor(private uiService: UiService, private route: Router) {}

  handleAddTask(): void {
    this.uiService.toggleAddTask();
  }

  ngOnInit(): void {
    this.uiService.onToggle().subscribe((val) => {
      this.showAddTask = val;
    });
  }

  hasRoute(route: string) {
    return this.route.url === route;
  }
}

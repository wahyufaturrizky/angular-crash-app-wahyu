import { Component, OnInit } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Task } from 'src/app/interface/Task';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
})
export class TaskComponent implements OnInit {
  task: Task[] = [];
  faTimes = faTimes;

  constructor(private taskService: TaskService) {}

  handleDeleteTask(e: Task): void {
    if (window.confirm('Are your sure want delete?')) {
      this.taskService.deleteTask(e).subscribe(
        (data) => console.log(data),
        (err) => window.alert(err.message),
        () => this.ngOnInit()
      );
    }
  }

  handleUpdateReminder(e: Task): void {
    e.reminder = !e.reminder;
    this.taskService.updateTask(e).subscribe(
      () => {},
      (err) => window.alert(err.message)
    );
  }

  onHandleSubmit(data: any): void {
    this.taskService.postTask(data).subscribe(
      () => window.alert('Success add new task'),
      (err) => console.log(err),
      () => this.ngOnInit()
    );
  }

  ngOnInit(): void {
    this.taskService.getTask().subscribe(
      (data) => (this.task = data),
      (err) => window.alert(err.message)
    );
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent implements OnInit {
  @Input() label: string = '';
  @Input() color: string = '';
  @Output() handleButtonClick = new EventEmitter();

  constructor() {}

  handleClick(): void {
    this.handleButtonClick.emit();
  }

  ngOnInit(): void {}
}

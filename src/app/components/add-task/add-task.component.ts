import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';

export interface dataFormTaskInterface {
  fieldName: string;
  fieldType: 'text' | 'checkbox';
  placeholder: string;
  fieldValue: string | boolean;
}

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit {
  isShowForm: boolean = false;
  @Output() onHandleSubmit: EventEmitter<dataFormTaskInterface[]> =
    new EventEmitter();
  objData: any = {
    task: '',
    day: '',
    reminder: false,
  };
  data: dataFormTaskInterface[] = [
    {
      fieldName: 'task',
      fieldType: 'text',
      placeholder: 'Add Task',
      fieldValue: '',
    },
    {
      fieldName: 'day',
      fieldType: 'text',
      placeholder: 'Add Day',
      fieldValue: '',
    },
    {
      fieldName: 'reminder',
      fieldType: 'checkbox',
      placeholder: 'Add Reminder',
      fieldValue: false,
    },
  ];

  handleSubmit(): void {
    let isEmptyField = this.data.find((finding) => finding.fieldValue === '');

    if (!isEmptyField) {
      this.onHandleSubmit.emit(this.objData);

      this.data = this.data.map((mapping) => ({
        ...mapping,
        fieldValue: mapping.fieldType === 'checkbox' ? false : '',
      }));
    } else {
      window.alert(`Please filling ${isEmptyField.fieldName} field!`);
    }
  }

  handleChange(e: dataFormTaskInterface): void {
    this.objData = { ...this.objData, [e.fieldName]: e.fieldValue };
  }

  constructor(private uiService: UiService) {}

  ngOnInit(): void {
    this.uiService.onToggle().subscribe((val) => (this.isShowForm = val));
  }
}

import { Routes } from '@angular/router';
import { TaskComponent } from './components/task/task.component';
import { AboutComponent } from './pages/about/about.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: TaskComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
];
